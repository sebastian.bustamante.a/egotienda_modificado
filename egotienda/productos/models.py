from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from autoslug import AutoSlugField #pa funcionar : pip install django_autoslug
from usuarios.models import Usuario

class Categoria(models.Model):
    titulo=models.CharField(max_length=200)
    slug=AutoSlugField(populate_from="titulo",unique=True)
    
    def __str__(self):
        return self.titulo

class Subcategoria(models.Model):
    categoria=models.ForeignKey(Categoria,related_name="products",on_delete=models.CASCADE)
    titulo=models.CharField(max_length=200,null=True)
    slug=AutoSlugField(populate_from="titulo",unique=True,null=True)

    def __str__(self):
        return self.titulo


class Producto(models.Model):
    categoria=models.ForeignKey(Categoria,related_name="producto",on_delete=models.CASCADE)
    sub_categoria=models.ForeignKey(Subcategoria,related_name="sub_categoria",on_delete=models.CASCADE)
    usuario=models.ForeignKey(Usuario,related_name="usuario_producto",on_delete=models.CASCADE)
    titulo=models.CharField(max_length=200)
    descripcion=models.TextField(max_length=1000)
    precio=models.DecimalField(max_digits=12,decimal_places=0)
    imagen=models.ImageField(upload_to="producto-")
    slug=AutoSlugField(populate_from="titulo",unique=True)

    def __str__(self):
        return self.titulo

