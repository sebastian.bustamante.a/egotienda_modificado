from django.conf.urls import url
from django.urls import path
from productos.views import ProductoView,ProductoViewPerson,FormularioCategoriaView,FormularioSubcategoriaView

urlpatterns = [
    path('',ProductoView.as_view(),name='index'),
    path('<slug:slug>',ProductoViewPerson.as_view(),name='producto'),
    path('formulario/',FormularioCategoriaView.as_view(),name='formulario'),
    path('formulario_2/',FormularioSubcategoriaView.as_view(),name='formulario_2')
]