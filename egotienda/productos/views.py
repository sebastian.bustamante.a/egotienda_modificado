from django.shortcuts import render
from productos.models import Categoria,Subcategoria,Producto
from productos.forms import CategoriaForm,SubcategoriaForm
# Create your views here.
from django.views.generic import TemplateView


class ProductoView(TemplateView):
    template_name = "productos/index.html"
    
    def get_context_data(self, **kwargs):
        kwargs['libros'] = Producto.objects.all()
        return kwargs
        

class ProductoViewPerson(TemplateView):
    template_name="productos/producto.html"
    def get_context_data(self, **kwargs):
        producto_slug=kwargs.get("slug")
        kwargs['libros'] = Producto.objects.get(slug=producto_slug)
        return kwargs
        

class FormularioCategoriaView(TemplateView):
    template_name = "productos/formulario_autor.html"

    def get_context_data(self, **kwargs):
        kwargs['form'] = CategoriaForm
        return kwargs

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save()
        context['form'] = form
        return self.render_to_response(context)


class FormularioSubcategoriaView(TemplateView):
    template_name = "productos/formulario_subcategoria.html"

    def get_context_data(self, **kwargs):
        kwargs['form'] = SubcategoriaForm
        return kwargs

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = SubcategoriaForm(request.POST)
        if form.is_valid():
            form.save()
        context['form'] = form
        return self.render_to_response(context)
